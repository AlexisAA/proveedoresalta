﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Activities.Expressions;

/// <summary>
/// Descripción breve de WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]



public class WebService : System.Web.Services.WebService
{


    private static string strKey = "llaveEncriptacionTFM";
    public WebService()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hola a todos";
    }
    [WebMethod]
    public string Proveedores(String ID_Proveedor_SAP, String Nombre, String RFC, String Direccion, String Telefono, String Pais, String Dias_Credito, String Banco, String Correo, String Logistico, String Financiero, String Tolerancia, String Extranjero, String  Sociedades)
    {
        String respuesta = "Correcto";
        List<string> lista = new List<string>();
        List<string> lista1 = new List<string>();
        List<string> lista2 = new List<string>();
        List<string> lista3 = new List<string>();
        List<string> lista4 = new List<string>();
        String co = "", co2 = "",co4="",co6="",co8="",socied="",socie="",sociedade="";
        int co1 = 0, co3 = 0, co5=0,co7=0,co9=0,count=0,dias_credito=0,logistico=0,financiero=0,tolerancia=0,extranjero=0;

        //Guarda de variables 
        String id_Proveedor_SAP = ID_Proveedor_SAP;
        if (id_Proveedor_SAP == "")
        {
            respuesta = "Por favor ingresa valores validos";
            
        }
        String nombre = Nombre;
        if (nombre == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String rfc = RFC;
        if (rfc == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String direccion = Direccion;
        if (direccion == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String telefono = Telefono;
        if (telefono == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String pais = Pais;
        if (pais == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String dias = Dias_Credito;
        if (dias == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        else
        {
            dias_credito = Int32.Parse(dias);
        }
        String banco = Banco;
        if (banco == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String correo = Correo;
        if (correo == "")
        {
            respuesta = "Por favor ingresa valores validos";

        }
        String contrasenia = "Portal01#";
        String lo = Logistico;
        if (lo == "")
        {
            respuesta = "Por favor ingresa valores validos";
        }
        else
        {
             logistico = Int32.Parse(lo);
        }
        String fi = Financiero;
        if (fi == "")
        {
            respuesta = "Por favor ingresa valores validos";
        }
        else
        {
            financiero = Int32.Parse(fi);
        }
        String to = Tolerancia;
        if (to == "")
        {
            respuesta = "Por favor ingresa valores validos";
        }
        else
        {
           tolerancia = Int32.Parse(to);
        }
        String ex = Extranjero;
        if (ex == "")
        {
            respuesta = "Por favor ingresa valores validos";
        }
        else
        {
            int extrajero = Int32.Parse(ex);
        }
        String sociedades = Sociedades;
        if (sociedades=="") {
            respuesta = "Por favor ingresa valores validos";
        }
        


        //encriptacion de contraseña
        byte[] keyArray;
        byte[] arrayCifrar = UTF8Encoding.UTF8.GetBytes(contrasenia);
        MD5CryptoServiceProvider hashMd5 = new MD5CryptoServiceProvider();
        keyArray = hashMd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(strKey));
        hashMd5.Clear();
        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        tdes.Key = keyArray;
        tdes.Mode = CipherMode.ECB;
        tdes.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = tdes.CreateEncryptor();
        byte[] arrayResultado = cTransform.TransformFinalBlock(arrayCifrar, 0, arrayCifrar.Length);
        tdes.Clear();
        String resul ="" + Convert.ToBase64String(arrayResultado,0,arrayResultado.Length) ;


        //seleccionar id_empresa
        String emp = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion = new SqlConnection(emp);
        conexion.Open();
        String con1 = "select id_empresa from TBL_Configuracion";
        SqlCommand comando = new SqlCommand(con1, conexion);
        SqlDataReader registro = comando.ExecuteReader();
        while (registro.Read())
        {
            co = registro[0].ToString();
            lista.Add(co);
            co1 = Int32.Parse(co);
        }
        conexion.Close();

        //fecha
        string dt = DateTime.Now.ToShortDateString();
        DateTime fecha = DateTime.Parse(dt);
        String dia = Convert.ToString(fecha.Day);
        String mes = Convert.ToString(fecha.Month);
        String año = Convert.ToString(fecha.Year);
        String fecha1 = año + "/" + mes + "/" + dia;

        //Insertar proveedor
        String pro = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion1 = new SqlConnection(pro);
        conexion1.Open();
        String con = "insert into TBL_Proveedores (ID_Proveedor_SAP, Nombre, RFC,Direccion,Telefono,Pais,Dias_Credito,Banco,Addenda,Portal,Correo,Fecha_Alta,Valido,Logistico,Financiero,Tolerancia,Extranjero,id_empresa) values (' " + id_Proveedor_SAP + "','" + nombre + "','" + rfc + "','" + direccion + "','" + telefono + "','" + pais + "'," + dias_credito + ",'" + banco + "', 0" + ", 1" + ",'" + correo + "','" + fecha1 + "', 1" + "," + logistico + "," + financiero + "," + tolerancia + "," + extranjero + "," + co1 + ")";
        SqlCommand comando1 = new SqlCommand(con, conexion1);
        comando1.ExecuteNonQuery();
        conexion1.Close();


        //Consulta Id_proveedor
        String prov = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion2 = new SqlConnection(prov);
        conexion2.Open();
        String con2 = "select MAX (ID_Proveedor) from TBL_Proveedores ";
        SqlCommand comando2 = new SqlCommand(con2, conexion2);
        SqlDataReader registro1 = comando2.ExecuteReader();
        while (registro1.Read())
        {
            co2 = registro1[0].ToString();
            lista1.Add(co2);
            co3 = Int32.Parse(co2);
        }
        conexion2.Close();

        //Insertar Usuario
        String us = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion3 = new SqlConnection(us);
        conexion3.Open();
        String con3 = "insert into TBL_Usuarios (Usuario,Contrasenia,Nombre,Fecha_Alta,Bloqueado,Fecha_Bloqueo,Acepta_Contrato,Fecha_Acpt_Contrato,FUltimo_Acceso,Activo,Valido,No_Intentos,ID_Proveedor,Correo,id_empresa,PrimeraSesion) values ('" + id_Proveedor_SAP + "','"+ resul +"','" + nombre + "','" + fecha1 +"',0,NULL,0,'"+ fecha1 +"',NULL,0,1,0,"+ co3 + ",'" + correo + "',"+ co1 + ",1)";
        SqlCommand comando3 = new SqlCommand(con3, conexion3);
        comando3.ExecuteNonQuery();
        conexion3.Close();

        //Consulta usuarios
        String usu = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion4 = new SqlConnection(usu);
        conexion4.Open();
        String con4 = "select MAX (ID_Usuario) from TBL_Usuarios";
        SqlCommand comando4 = new SqlCommand(con4,conexion4);
        SqlDataReader registro2 = comando4.ExecuteReader();
        while (registro2.Read())
        {
            co4 = registro2[0].ToString();
            lista2.Add(co4);
            co5 = Int32.Parse(co4);
        }
        conexion4.Close();

        //Inserta usuario en perfil
        String usp = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion5 = new SqlConnection(usp);
        conexion5.Open();
        String con5 = "insert into TBL_UsuarioenPerfil (ID_Usuario,ID_Perfil,Valido,id_empresa) values ("+co5+",2,1,"+co1+")";
        SqlCommand comando5 = new SqlCommand(con5, conexion5);
        comando5.ExecuteNonQuery();
        conexion5.Close();

        //consulta de sociedades 
        String so = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion6 = new SqlConnection(so);
        conexion6.Open();
        String con6 = "select count (*) from TBL_Sociedades";
        SqlCommand comando6 = new SqlCommand(con6, conexion6);
        SqlDataReader registro3 = comando6.ExecuteReader();
        while (registro3.Read())
        {
            co6 = registro3[0].ToString();
            lista3.Add(co6);
            co7 = Int32.Parse(co6); 
        }
        conexion6.Close();

        //consulta de ID_sociedades
        String soc = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
        SqlConnection conexion7 = new SqlConnection(soc);
        List<string> ListaSoc = sociedades.Split(',').ToList();
        int rows = 0;
        foreach (string sociedad in ListaSoc)
        {
            conexion7.Open();
            String con7 = "select Id_Sociedad from TBL_Sociedades where Id_Sociedad_SAP=" + sociedad;
            SqlCommand comando7 = new SqlCommand(con7, conexion7);
            SqlDataReader registro4 = comando7.ExecuteReader();
            while (registro4.Read())
            {
                co8 = registro4[0].ToString();
                lista4.Add(co8);
                co9 = Int32.Parse(co8);
            }
            conexion7.Close();
            //inserta Proveedor en sociedad
            String prso = System.Configuration.ConfigurationManager.ConnectionStrings["cadenaconexion1"].ConnectionString;
            SqlConnection conexion8 = new SqlConnection(prso);
            conexion8.Open();
            String con8 = "insert into TBL_ProveedorenSociedad (Id_Proveedor,Id_Sociedad,Valido,id_empresa) values (" + co3 + "," + co9 + ",1," + co1 + ")";
            SqlCommand comando8 = new SqlCommand(con8, conexion8);
            rows += comando8.ExecuteNonQuery();
            conexion8.Close();
        }
        //Enviar correo
        string respEnvio = string.Empty;
        string strServer = ConfigurationManager.AppSettings["mailServer"].ToString();
        string strRemitente = ConfigurationManager.AppSettings["mailRemitente"].ToString();
        string strContrasenia = ConfigurationManager.AppSettings["mailContrasenia"].ToString();
        int intPuerto = Convert.ToInt32(ConfigurationManager.AppSettings["mailPuerto"].ToString());
        bool ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["mailSSL"].ToString());

        SmtpClient datosSmtp = new SmtpClient();
        datosSmtp.Credentials = new System.Net.NetworkCredential(strRemitente, strContrasenia);
        datosSmtp.Host = strServer;
        datosSmtp.Port = intPuerto;
        datosSmtp.EnableSsl = ssl;

        MailMessage objetoMail = new MailMessage();
        MailAddress mailRemitente = new MailAddress(strRemitente);
        objetoMail.From = mailRemitente;

        string mailAsunto = "";
        string strMensajeHTML = "";

        mailAsunto = "Portal de Proveedores";

        strMensajeHTML = "<html style='font-size: 12px; font-family: sans-serif Arial; margin: 0; padding: 0; text-align: center; vertical-align: top; background: #ffffff;'>";
        strMensajeHTML += "<head><title></title><meta charset='utf-8' /></head>";
        strMensajeHTML += "<body style='width: 100%; height: 100%; margin: 0; padding: 0; text-align: left; vertical-align: top; margin-left: 10px;'>";
        strMensajeHTML += "<center><p style='font-size: 26px; font-weight: bold; color: #000000;'><img src=http://201.166.145.206/portalammpercalidad/Ammper_Logo.png /><br />Portal de Proveedores</center>";
        strMensajeHTML += "<p style='font-size: 15px; font-weight: bold; color: #000000;'>Estimado Proveedor de Ammper: </p>";
        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; color: #000000;'>Con el objetivo de hacer más eficiente nuestra gestión relacionada con la programación y consultas de pagos, hemos implementado un nuevo <strong>Portal de Proveedores </strong>a partir del <strong>XX de abril de 2019</strong>, el cual es el canal oficial para recibir los documentos fiscales conforme a la legislación vigente (Facturas; Notas de Crédito; etc.).</p></div>";
        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; color: #000000;'>La información de pago de documentos será obtenida directamente por el Portal de Proveedores http://www.proveedores.ammper.mx . Para acceder a este sitio,  cada Proveedor deberá ingresar un usuario y una clave (las cuales contienen ciertas reglas de seguridad), dicho usuario y clave se debe solicitar al Administrador del Portal y el Proveedor podrá visualizar los datos y confirmar que sean correctos o pedir alguna modificación en caso de ser necesario; no será posible mantener más de una sesión de forma simultánea; además de poder elegir los idiomas (Inglés/Español).</p></div>";
        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; color: #000000;'>Cada proveedor y/o usuario podrá enviar la (s) factura (s) para iniciar con el proceso de validaciones fiscales, para posteriormente especificar la fecha de programación de los pagos. Nuestras políticas y condiciones podrán ser consultadas en dicho Portal.</p></div>";

        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; color: #000000;'>Se le ha asignado el siguiente usuario: <strong>" + id_Proveedor_SAP + "</strong> y contraseña: <strong>" + contrasenia + "</strong></p></div>";

        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; color: #000000;'>Sin más por el momento, reciba un cordial saludo,</p></div>";

        strMensajeHTML += "<div ALIGN=justify><p style='font-size: 14px; font-weight: bold; color: #000000;'>ATENTAMENTE,<br />Cuentas por Pagar</p></div>";
        strMensajeHTML += "</body></html>";

        //string mailTo = correo;
        string mailTo = "arredondoacevesalexis@gmail.com";
        objetoMail.To.Add(mailTo);

        objetoMail.Subject = mailAsunto;
        objetoMail.Body = strMensajeHTML;
        objetoMail.IsBodyHtml = true;

        
        string rutapdf = @"C:\Users\Alexis\source\repos\proveedoresAlta\Manual de Usuario Portal Proveedores.pdf";

        try
        {
            if (File.Exists(rutapdf))
            {
                Attachment adjuntoPDF = new Attachment(rutapdf);
                objetoMail.Attachments.Add(adjuntoPDF);
            }
        }
        catch (Exception )
        {
            
        }

        datosSmtp.Send(objetoMail);
        datosSmtp.Dispose();

        return respuesta+co7+co9;
    }
}
